# Test React / Node JS - eKonsilio

Ce dépôt contient le Front-end en ReactJs et le Back-end avec NodeJs de l'application.

## Installation des paquets

    1. Back-end
```shell
cd api
yarn install
```

    2. Front-end
```shell
cd app
yarn install
```

## Exécution en local

    1. Back-end
```shell
cd api
node server.js
```

    2. Front-end
```shell
cd app
yarn start
```
