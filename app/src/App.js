import React, { Component } from "react";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./styles/App.css";

import ChatsList from "./components/Chats-list.component";
import AddChat from "./components/AddChat.component";

class App extends Component {
    render() {
        return (
            <Router>
                <div>
                    <nav className="navbar navbar-expand navbar-dark bg-dark">
                        <Link to={"/chats?status=1"} className="navbar-brand">
                            RadoChatTestApp
                        </Link>
                        <div className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link to={"/chats?status=1"} className="nav-link">
                                    Accueil
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link to={"/chats?status=0"} className="nav-link">
                                    Conversations archivées
                                </Link>
                            </li>
                        </div>
                        <div className="navbar-nav ms-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <Link to={"/add"} className="nav-link">
                                    Créer une conversation
                                </Link>
                            </li>
                        </div>
                    </nav>

                    <div className="container mt-3">
                        <Route exact path={["/", "/chats"]} component={ChatsList} />
                        <Route exact path={["/add"]} component={AddChat} />
                        <Route path="/chats/:id" component={AddChat} />
                    </div>
                </div>
            </Router>
        );
    }
}

export default App;
