import React, { Component } from "react";
import { Link } from "react-router-dom";
import ChatDataService from "../services/chat.service";
import "./../styles/Chats-list.css"
import Avatar from 'react-avatar';
import SimpleReactValidator from 'simple-react-validator';

export default class ChatsList extends Component {
    constructor(props) {
        super(props);
        this.retrieveChats = this.retrieveChats.bind(this);
        this.refreshList = this.refreshList.bind(this);
        this.setActiveChat = this.setActiveChat.bind(this);
        this.removeAllChats = this.removeAllChats.bind(this);
        this.onChangeSenderName = this.onChangeSenderName.bind(this);
        this.onChangeMessage = this.onChangeMessage.bind(this);
        this.saveMessage = this.saveMessage.bind(this);
        this.closeChat = this.closeChat.bind(this);
        this.state = {
            chats: [],
            messages: [],
            currentChat: null,
            currentIndex: -1,
            senderName: '',
            message: '',
            status: '1'
        };
        this.validator = new SimpleReactValidator({
            messages: {
                required: 'Ce champ est requis'
            }
        });
    }

    componentDidMount() {
        this.retrieveChats(this.state.status);
    }

    componentDidUpdate(prevProps) {
        if (this.props.location !== prevProps.location) {
            this.onRouteChanged();
        }
    }

    onRouteChanged = async () => {
        await this.setState({ status: new URLSearchParams(this.props.location.search).get("status") });
        await this.setState({ currentChat: null });
        await this.setState({ currentIndex: -1 });
        this.retrieveChats(this.state.status);
    }

    retrieveChats(status) {
        ChatDataService.getAll(status)
            .then(response => {
                this.setState({
                    chats: response.data.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    retrieveMessagesByChatId(chatId) {
        ChatDataService.getMessagesByChat(chatId)
            .then(response => {
                this.setState({
                    messages: response.data.data
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    refreshList() {
        this.retrieveChats(this.state.status);
        this.setState({
            currentChat: null,
            currentIndex: -1
        });
    }

    setActiveChat(chat, index) {
        this.retrieveMessagesByChatId(chat._id);
        this.setState({
            currentChat: chat,
            currentIndex: index
        });
    }

    removeAllChats() {
        ChatDataService.deleteAll()
            .then(response => {
                console.log(response.data);
                this.refreshList();
            })
            .catch(e => {
                console.log(e);
            });
    }

    onChangeSenderName(e) {
        this.setState({
            senderName: e.target.value
        });
    }

    onChangeMessage(e) {
        this.setState({
            message: e.target.value
        });
    }

    saveMessage() {
        if (this.validator.allValid()) {
            const data = {
                chatId: this.state.currentChat._id,
                senderName: this.state.senderName,
                message: this.state.message
            };
            ChatDataService.createMessage(data)
                .then(response => {
                    this.setState({
                        senderName: '',
                        message: ''
                    });
                    this.retrieveMessagesByChatId(this.state.currentChat._id);
                })
                .catch(e => {
                    console.log(e);
                });
        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }
    }

    closeChat() {
        const data = {
            active: false
        }
        ChatDataService.close(this.state.currentChat._id, data)
            .then(response => {
                this.refreshList();
            })
            .catch(e => {
                console.log(e);
            });
    }

    render() {
        const { chats, currentChat, currentIndex, messages, status } = this.state;
        return (
            <div className="list row">
                <div className="col-md-4">
                    <h4>{status === '1' ? 'Liste des conversations' : 'Conversations archivées'}</h4>
                    <ul className="list-group">
                        {chats &&
                            chats.map((chat, index) => (
                                <li
                                    className={
                                        "list-group-item " +
                                        (index === currentIndex ? "active" : "")
                                    }
                                    onClick={() => this.setActiveChat(chat, index)}
                                    key={index}
                                >
                                    {chat.title}
                                    <span className="float-right"></span>
                                </li>
                            ))}
                    </ul>
                    <Link to={"/add"} className="mt-3 btn btn-sm btn-primary">Créer une conversation</Link>
                </div>
                <div className="col-md-8 messageContainer">
                    {currentChat ? (
                        <div>
                            <div className="row">
                                <div className="col-6"><h4 className="mt-3 mb-3">{currentChat.title.toUpperCase()}</h4></div>
                                {status === '1' && <div className="col-6 text-end">
                                    <div className="d-grid gap-2 d-md-flex justify-content-md-end">
                                        <Link to={`/chats/${currentChat._id}`} className="mt-3 btn btn-sm btn-primary me-md-2">Modifier</Link>
                                        <button onClick={this.closeChat} className="mt-3 btn btn-sm btn-danger">Fermer la conversation</button>
                                    </div>
                                </div>}
                            </div>
                            {messages && messages.map((msg) => (
                                <div className="row messageItem" key={msg._id}>
                                    <div className="col-1 no-padding-right">
                                        <Avatar name={msg.senderName} size={46} round="2px" textSizeRatio={1.5} />
                                    </div>
                                    <div className="col-10 no-padding-left">
                                        &nbsp;<b>{msg.senderName}</b>&nbsp;{new Date(msg.createdAt).toLocaleDateString()}&nbsp;{new Date(msg.createdAt).toLocaleTimeString()}
                                        <br />
                                        &nbsp;{msg.message}
                                    </div>
                                </div>
                            ))}
                            {status === '1' ? (
                                <div>
                                    <div className="form-group">
                                        <label htmlFor="senderName">Votre nom *:</label>
                                        <input type="text" id="senderName" name="senderName" className="form-control" value={this.state.senderName} onChange={this.onChangeSenderName} maxLength="50" />
                                        {this.validator.message('senderName', this.state.senderName, 'required')}
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="message">Votre message *:</label>
                                        <textarea id="message" name="message" className="form-control" value={this.state.message} onChange={this.onChangeMessage} rows="3" maxLength="255"></textarea>
                                        {this.validator.message('message', this.state.message, 'required')}
                                    </div>
                                    <button onClick={this.saveMessage} className="mt-3 mb-3 btn btn-sm btn-primary">Envoyer</button>
                                </div>
                            ) : null}

                        </div>
                    ) : chats && chats.length ? (
                        <div>
                            <br />
                            <p>Cliquer sur une conversation s'il vous plaît...</p>
                        </div>
                    ) : null}
                </div>
            </div>
        );
    }
}