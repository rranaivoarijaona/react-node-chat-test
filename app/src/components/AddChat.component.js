import React, { Component } from "react";
import { Link } from "react-router-dom";
import SimpleReactValidator from "simple-react-validator";
import ChatDataService from "../services/chat.service";

export default class AddChat extends Component {
    constructor(props) {
        super(props);
        this.onChangeTitle = this.onChangeTitle.bind(this);
        this.saveChat = this.saveChat.bind(this);
        this.newChat = this.newChat.bind(this);

        this.state = {
            id: null,
            title: '',
            submitted: false
        };
        this.validator = new SimpleReactValidator({
            messages: {
                required: 'Ce champ est requis'
            }
        });
    }

    componentDidMount() {
        const chatId = this.props.match.params.id;
        if (chatId) {
            this.getChat(chatId);
        }
    }

    onChangeTitle(e) {
        this.setState({
            title: e.target.value
        });
    }

    getChat(id) {
        ChatDataService.get(id)
            .then(response => {
                this.setState({
                    id: response.data.data._id,
                    title: response.data.data.title
                });
            })
            .catch(e => {
                console.log(e);
            });
    }

    saveChat() {
        if (this.validator.allValid()) {
            let request;
            if (this.state.id) {
                request = ChatDataService.modify(this.state.id, {
                    title: this.state.title
                })

            } else {
                request = ChatDataService.create({
                    title: this.state.title
                })
            }
            request.then(response => {
                this.props.history.push('/chats?status=1')
            })
                .catch(e => {
                    console.log(e);
                });
        } else {
            this.validator.showMessages();
            this.forceUpdate();
        }
    }

    newChat() {
        this.setState({
            id: null,
            title: '',
            submitted: false
        });
    }

    render() {
        return (
            <div className="submit-form">
                {this.state.submitted ? (
                    <div>
                        <h4>Nouvelle conversation envoyé!</h4>
                        <button className="m-3 btn btn-success" onClick={this.newChat}>
                            Ajouter
                        </button>
                    </div>
                ) : (
                    <div>
                        <h4>{this.state.id ? `Modification de ${this.state.title}` : `Création d'une nouvelle conversation`}</h4>
                        <div className="form-group">
                            <label htmlFor="title">Titre</label>
                            <input
                                type="text"
                                className="form-control"
                                id="title"
                                required
                                value={this.state.title}
                                onChange={this.onChangeTitle}
                                name="title"
                                maxLength="50"
                            />
                            {this.validator.message('title', this.state.title, 'required')}
                        </div>
                        <button onClick={this.saveChat} className="mt-3 btn btn-primary">Enregistrer</button>
                        <Link to={"/chats?status=1"} className="mt-3 btn btn-light">Annuler</Link>
                    </div>
                )}
            </div>
        );
    }
}