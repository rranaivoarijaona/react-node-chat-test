import http from "../http-common";

class ChatDataService {
    getAll(status) {
        return http.get(`/chat?status=${status}`);
    }

    get(id) {
        return http.get(`/chat/${id}`);
    }

    create(data) {
        return http.post("/chat", data);
    }

    modify(id, data) {
        return http.put(`/chat/${id}`, data);
    }

    close(id, data) {
        return http.put(`/chat/${id}`, data);
    }

    deleteAll() {
        return http.delete(`/chats`);
    }

    createMessage(data) {
        return http.post(`/chat/${data.chatId}/message`, data);
    }

    getMessagesByChat(chatId) {
        return http.get(`/chat/${chatId}/messages`);
    }
}

export default new ChatDataService();