const mongoose = require("mongoose");

function mongooseConnectDB(uri) {
    mongoose.connect(uri,
        {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        .then((result) => console.log('Connexion à MongoDB réussie !', result.connections[0].host))
        .catch((err) => console.log('Connexion à MongoDB échouée !', err));
}

module.exports = mongooseConnectDB;