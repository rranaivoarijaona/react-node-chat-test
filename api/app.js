const express = require('express');
const app = express();
const cors = require('cors');
const chatRoutes = require('./routes/chat.js');
const dotenv = require('dotenv').config({
    path: './.env'
});

const mongooseConnectDB = require("./config/mongoose.db.config");
mongooseConnectDB(process.env.DB_URL);

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.get('/', (req, res) => {
    res.status(200).send(`Welcome to chat test API`);
});
app.use('/api/chat', chatRoutes);

module.exports = app;