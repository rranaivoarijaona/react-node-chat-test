const express = require('express');
const router = express.Router();
const chatCtrl = require('../controllers/chat');

router.get('/', chatCtrl.getAll);
router.post('/', chatCtrl.create);
router.put('/:id', chatCtrl.modify);
router.get('/:id', chatCtrl.get);
router.get('/:id/messages', chatCtrl.getMessages);
router.post('/:id/message', chatCtrl.createMessage);

module.exports = router;