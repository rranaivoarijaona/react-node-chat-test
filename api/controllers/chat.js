const Chat = require('./../models/chat');
const Message = require('./../models/message');

module.exports = {
    getAll: async (req, res) => {
        try {
            const chats = await Chat.find({ active: +req.query.status === 1 ? true : false });
            res.status(200).json({ data: chats, message: 'All chats retrieved!' });
        } catch (error) {
            console.log('error', error);
            res.status(500).json({ error, message: 'Error when getting all chats!' })
        }
    },
    get: async (req, res) => {
        try {
            const chat = await Chat.findOne({ _id: req.params.id });
            res.status(200).json({ data: chat, message: 'Chat retrieved!' });
        } catch (error) {
            console.log('error', error);
            res.status(500).json({ error, message: 'Error when getting chat!' })
        }
    },
    create: async (req, res) => {
        delete req.body._id;
        const chat = new Chat({
            ...req.body
        });
        try {
            const chats = await chat.save();
            res.status(201).json({ data: chats, message: 'New chat saved!' });
        } catch (error) {
            console.log('error', error);
            res.status(500).json({ error, message: 'Error when creating a chat!' })
        }
    },
    modify: async (req, res) => {
        try {
            const chats = await Chat.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id });
            res.status(201).json({ data: chats, message: 'chat updated!' });
        } catch (error) {
            console.log('error', error);
            res.status(500).json({ error, message: 'Error when updating a chat!' })
        }
    },
    createMessage: async (req, res) => {
        delete req.body._id;
        try {
            const chat = await Chat.findOne({ _id: req.body.chatId });
            if (!chat) {
                return res.status(500).json({ message: 'Chat did not found!' });
            }
            const message = new Message({
                ...req.body
            });
            const messages = chat.messages ? [...chat.messages, message] : [message];
            const chats = await Chat.updateOne({ _id: req.body.chatId }, { messages });
            const savedMessage = await message.save();
            res.status(201).json({ data: { savedMessage, chats }, message: 'New message saved!' });
        } catch (error) {
            console.log('error', error);
            res.status(500).json({ error, message: 'Error when creating a message!' })
        }
    },
    getMessages: async (req, res) => {
        try {
            const chats = await Message.find({ chatId: req.params.id });
            res.status(200).json({ data: chats, message: 'All messages retrieved!' });
        } catch (error) {
            console.log('error', error);
            res.status(500).json({ error, message: `Error when getting all message for chat ${req.params.id}` })
        }
    },
    close: async (req, res) => {

    }
};