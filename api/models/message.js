const mongoose = require('mongoose');

const MessageSchema = mongoose.Schema({
    chatId: { type: String, required: true },
    senderName: { type: String, required: true },
    message: { type: String, required: true }
},
    {
        timestamps: true
    });

module.exports = mongoose.model('Message', MessageSchema);