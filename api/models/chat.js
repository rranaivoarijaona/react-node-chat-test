const mongoose = require('mongoose');

const ChatSchema = mongoose.Schema({
    title: { type: String, required: true },
    active: { type: Boolean, default: true },
    messages: [mongoose.Schema({
        chatId: { type: String, required: true },
        senderName: { type: String, required: true },
        message: { type: String, required: true }
    })]
},
    {
        timestamps: true
    });

module.exports = mongoose.model('Chat', ChatSchema);